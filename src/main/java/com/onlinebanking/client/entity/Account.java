package com.onlinebanking.client.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
@Data
@Document(collection = "Cards")
public class Account {
    private String userid;

    private String accountNumber;

    private String expireDate;

    private String balance;

    private boolean isActive;
}
