package com.onlinebanking.client.entity;
import java.util.Random;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "onlinebank-client")
public class Client {
    @Id
    private String id;


    private String userid;

    private String fullname;

    private String email;

    private String phone;

    private String login;

    private String password;

}