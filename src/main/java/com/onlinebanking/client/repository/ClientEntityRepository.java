package com.onlinebanking.client.repository;

import com.onlinebanking.client.entity.Client;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClientEntityRepository extends MongoRepository <Client,String> {
}
