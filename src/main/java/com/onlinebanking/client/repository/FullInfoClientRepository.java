package com.onlinebanking.client.repository;

import com.onlinebanking.client.dto.fullinfo.FullinfoUserResponse;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FullInfoClientRepository extends MongoRepository<FullinfoUserResponse,String> {
}
