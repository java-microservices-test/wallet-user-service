package com.onlinebanking.client.repository;

import com.onlinebanking.client.dto.register.RegisterRequest;
import com.onlinebanking.client.entity.Client;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClientRepository extends MongoRepository <Client,String>{
    public Client findFirstById(String id);

    public boolean existsByLoginAndPhone(RegisterRequest request);


    public Client findFirstByLoginOrPhoneOrEmail(String login,String phone,String email);

    public Client findFirstByLogin (String login);

    public Client findFirstByEmail (String email);

    public Client findFirstByPhone (String phone);

    public Client findFirstByUserid(String userid);
}
