package com.onlinebanking.client.repository;

import com.onlinebanking.client.entity.AuthData;
import com.onlinebanking.client.entity.Client;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ClientAuthEntityRepository extends MongoRepository<AuthData,String> {
    public AuthData findFirstByLoginAndPassword(String login,String password);

    public  AuthData getAllByLogin(String login);

}
