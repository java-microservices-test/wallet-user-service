package com.onlinebanking.client.repository;

import com.onlinebanking.client.entity.Account;
import com.onlinebanking.client.entity.Client;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository<Account,String> {

    public Account findAllByUserid(String userid);

}
