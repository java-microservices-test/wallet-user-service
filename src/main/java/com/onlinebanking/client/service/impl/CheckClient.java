package com.onlinebanking.client.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.onlinebanking.client.dto.account.AccountDto;
import com.onlinebanking.client.dto.login.LoginRequest;
import com.onlinebanking.client.dto.login.LoginResponse;
import com.onlinebanking.client.dto.register.RegisterRequest;
import com.onlinebanking.client.dto.register.RegisterResponse;
import com.onlinebanking.client.entity.Account;
import com.onlinebanking.client.entity.AuthData;
import com.onlinebanking.client.entity.Client;
import com.onlinebanking.client.kafka.KafkaListenTopic;
import com.onlinebanking.client.mappper.RequestMapper;
import com.onlinebanking.client.repository.AccountRepository;
import com.onlinebanking.client.repository.ClientAuthEntityRepository;
import com.onlinebanking.client.repository.ClientEntityRepository;
import com.onlinebanking.client.repository.ClientRepository;
import com.onlinebanking.client.service.ClientService;
import com.onlinebanking.client.service.GenerateDataForAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class CheckClient implements ClientService {
    @Autowired
    private final ClientRepository clientRepository;
    @Autowired
    private KafkaListenTopic kafkaListenTopic;
    @Autowired
    private ClientEntityRepository clientEntityRepository;
    @Autowired
    RequestMapper requestMapper;

    @Autowired
    private ClientAuthEntityRepository clientAuthEntityRepository;
    @Autowired
    private AccountRepository accountRepository;

    public CheckClient(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    private static final Logger log = LoggerFactory.getLogger(CheckClient.class);


    public RegisterResponse saveUser(RegisterRequest registerRequest) throws JsonProcessingException {
        log.info("Saving user process is started-->");
        String stringUserid = String.format("%08d", new Random().nextInt(100000000));
        Client client = requestMapper.toClient(registerRequest);
        RegisterResponse registerResponse = new RegisterResponse();
        boolean searchUserTrue = true;
        log.info("Search active user for registration started-->");
        if (!searchUser(registerRequest)) {
            client = activeUserBuilder(registerRequest);
            client.setUserid(stringUserid);
            log.info("Search active user for registration: {},", searchUserTrue);
            log.info("Saving user in db");
            clientEntityRepository.save(client);

            AccountDto accountDto = new AccountDto();
            Account account = generateAccountInfo(accountDto);
            account.setUserid(stringUserid);
            accountRepository.save(account);

            AuthData authData = new AuthData();
            authData = authUserDataBuilder(registerRequest);
            authData.setUserid(stringUserid);
            log.info("Saving authentication user data in db");
            clientAuthEntityRepository.save(authData);
            log.info("Saving user was successful.");
        }
        return registerResponse;

    }

    public boolean searchUser(RegisterRequest request) {
        boolean isNotFind = false;

        if (clientRepository.findFirstByLogin(request.getLogin()) != null) {
            log.info("Login is unavailable: {},", request.getLogin());
            isNotFind = true;
        } else {
            log.info("Login is available: {}", request.getLogin());
        }
        if (clientRepository.findFirstByEmail(request.getEmail()) != null) {
            log.info("Email is unvailable: {}", request.getEmail());
            isNotFind = true;
        } else {
            log.info("Email is available: {}", request.getEmail());
        }
        if (clientRepository.findFirstByPhone(request.getPhone()) != null) {
            log.info("Phone is unavailable:{}", request.getPhone());
            isNotFind = true;
        } else {
            log.info("Phone is available: {}", request.getPhone());
        }
        return isNotFind;
    }

    public Boolean getHashPassword(LoginRequest loginRequest) {
        log.info("Validate password");
        AuthData authData = clientAuthEntityRepository.getAllByLogin(loginRequest.getLogin());
        if (authData == null) {
            log.info("LOGIN NOT FOUND");
            return false;
        } else {
            log.info("LOGIN IS FOUND");
            LoginResponse loginResponse = new LoginResponse();
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            boolean passwordMatch = encoder.matches(loginRequest.getPassword(), authData.getPassword());
            loginResponse.setPassword(authData.getPassword());// :)))))))))))))))
            log.info("Validate password: {}", passwordMatch);
            return passwordMatch;
        }

    }

    public Client activeUserBuilder(RegisterRequest registerRequest) {
        Client client = new Client();
        client.setLogin(registerRequest.getLogin());
        client.setFullname(registerRequest.getFullname());
        client.setEmail(registerRequest.getEmail());
        client.setPhone(registerRequest.getPhone());
        return client;
    }

    public AuthData authUserDataBuilder(RegisterRequest registerRequest) {
        AuthData authData = new AuthData();
        authData.setLogin(registerRequest.getLogin());
        authData.setPassword(registerRequest.getPassword());
        return authData;
    }

    public Account generateAccountInfo(AccountDto accountDto) {
        Random random = new Random();
        String acc = "44" + String.format("%08d", new Random().nextInt(1000000000));
        int month = ThreadLocalRandom.current().nextInt(1, 13);
        int year = ThreadLocalRandom.current().nextInt(2022, 2032);
        // Создаем объект LocalDate
        LocalDate date = LocalDate.of(year, month, 1);
        // Форматируем дату в нужный формат
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-yyyy");
        String expireDate = formatter.format(date);
        String balance = "15000";
        Account account = new Account();
        account.setAccountNumber(acc);
        account.setExpireDate(expireDate);
        account.setBalance(balance);
        return account;

    }

}
