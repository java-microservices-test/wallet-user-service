package com.onlinebanking.client.service.impl;

import com.onlinebanking.client.dto.fullinfo.FullinfoUserResponse;
import com.onlinebanking.client.entity.Account;
import com.onlinebanking.client.entity.Client;
import com.onlinebanking.client.repository.AccountRepository;
import com.onlinebanking.client.repository.ClientRepository;
import com.onlinebanking.client.service.FullInfoClientService;
import org.mapstruct.factory.Mappers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FullInfoClientServiceImpl implements FullInfoClientService {
    private static final Logger log = LoggerFactory.getLogger(CheckClient.class);

    private Client client;

    private Account account;
    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private AccountRepository accountRepository;

    public FullinfoUserResponse fullinfoUser(String userid) {
        FullinfoUserResponse fullinfoUserResponse = new FullinfoUserResponse();
        client = clientRepository.findFirstByUserid(userid);
        account = accountRepository.findAllByUserid(userid);
        fullinfoUserResponse.setUserid(client.getUserid());
        fullinfoUserResponse.setFullname(client.getFullname());
        fullinfoUserResponse.setEmail(client.getEmail());
        fullinfoUserResponse.setPhone(client.getPhone());
        fullinfoUserResponse.setAccountNumber(account.getAccountNumber());
        fullinfoUserResponse.setBalance(account.getBalance());
        fullinfoUserResponse.setExpireDate(account.getExpireDate());
        fullinfoUserResponse.setActive(account.isActive());
        log.info("search fullInfo");
        if(client == null && account==null){
            log.info("NOT FOUND");
        }
        else {
            log.info("FOUND");
        }
        return fullinfoUserResponse;

    }}
