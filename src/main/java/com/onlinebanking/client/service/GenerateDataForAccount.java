package com.onlinebanking.client.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class GenerateDataForAccount {
    public void  generateAccountInfo(String acc,String expireDate,String balance){
        Random random = new Random();
        acc = "44" +  String.format("%08d", new Random().nextInt(1000000000));
        int month = ThreadLocalRandom.current().nextInt(1, 13);
        int year = ThreadLocalRandom.current().nextInt(2022, 2032);
        // Создаем объект LocalDate
        LocalDate date = LocalDate.of(year, month, 1);
        // Форматируем дату в нужный формат
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-yyyy");
        expireDate = formatter.format(date);
        balance = "15000";

    }
}
