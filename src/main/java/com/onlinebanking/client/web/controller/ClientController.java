package com.onlinebanking.client.web.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.onlinebanking.client.dto.fullinfo.FullAccountInfo;
import com.onlinebanking.client.dto.fullinfo.FullinfoUserResponse;
import com.onlinebanking.client.dto.login.LoginRequest;
import com.onlinebanking.client.dto.login.LoginResponse;
import com.onlinebanking.client.dto.register.RegisterRequest;
import com.onlinebanking.client.dto.register.RegisterResponse;
import com.onlinebanking.client.kafka.KafkaListenTopic;
import com.onlinebanking.client.service.impl.CheckClient;
import com.onlinebanking.client.service.impl.FullInfoClientServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/")
public class ClientController {
    @Autowired
    private CheckClient checkClient;
    @Autowired
    private KafkaListenTopic kafkaListenTopic;
    @Autowired
    private FullInfoClientServiceImpl fullInfoClientService;

    /* @GetMapping("/users/")
     public Client clientSave(Client client) throws JsonProcessingException {
         return checkClient.checkClient(client);
     }*/
    @PostMapping(value = "/users/checkregistration/")
    RegisterResponse checkReg(@RequestBody RegisterRequest registerRequest) throws JsonProcessingException {
        return checkClient.saveUser(registerRequest);
    }

    @PostMapping(value = "/login/check/")
    boolean loginUser(@RequestBody LoginRequest loginRequest) throws JsonProcessingException {
        return checkClient.getHashPassword(loginRequest);
    }

    @GetMapping(value = "/users/{userid}/info/")
    public FullinfoUserResponse getFullInfo(@PathVariable String userid){
        return fullInfoClientService.fullinfoUser(userid);
    }
}
