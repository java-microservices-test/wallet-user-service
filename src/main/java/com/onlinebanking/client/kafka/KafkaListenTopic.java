package com.onlinebanking.client.kafka;

import com.onlinebanking.client.entity.Client;
import com.onlinebanking.client.mappper.EntityMapper;
import com.onlinebanking.client.repository.ClientRepository;
import com.onlinebanking.client.service.impl.CheckClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Collections;
import java.util.List;


@Service
public class KafkaListenTopic {
    private static final Logger log = LoggerFactory.getLogger(KafkaListenTopic.class);
    @Autowired
    ClientRepository clientRepository;

    @KafkaListener(topics = "wallet-users",groupId = "myGroup")
    public void consume(List <String> clients) throws IOException {
        CheckClient checkClient = new CheckClient(clientRepository);
        log.info(String.format("Message recieved -> %s", clients));
        EntityMapper mapper = new EntityMapper();
        Client client = mapper.TopicMessageToDto(clients);
    }

}
