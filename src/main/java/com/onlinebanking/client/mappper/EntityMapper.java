package com.onlinebanking.client.mappper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.onlinebanking.client.dto.ClientAuthDataDto;
import com.onlinebanking.client.dto.ClientDto;
import com.onlinebanking.client.entity.AuthData;
import com.onlinebanking.client.entity.Client;


import java.util.List;


public class EntityMapper  { ;


    public  Client TopicMessageToDto (List<String> message) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String messageJson = String.join(",",message);
        ClientDto clientDto = mapper.readValue(messageJson,ClientDto.class);
        Client client = new Client();
        client.setId(clientDto.getId());
        client.setFullname(clientDto.getFullname());
        client.setEmail(clientDto.getEmail());
        client.setPhone(clientDto.getPhone());
        client.setLogin(clientDto.getLogin());
        return client;
    }
    public ClientDto RegisterDataToDto(String message) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        ClientDto clientDto = new ClientDto();
        ClientDto registerDto1 = mapper.readValue(message, ClientDto.class);;
        clientDto.setLogin(registerDto1.getLogin());
        clientDto.setPassword(registerDto1.getPassword());
        clientDto.setEmail(registerDto1.getEmail());
        clientDto.setPhone(registerDto1.getPhone());
        clientDto.setPassword(registerDto1.getPassword());
        clientDto.setFullname(registerDto1.getFullname());
        return clientDto;
    };



}
