package com.onlinebanking.client.dto.fullinfo;

import lombok.Data;

@Data
public class FullAccountInfo {
    private String userid;

    private String accountNumber;

    private String expireDate;

    private String balance;

    private boolean isActive;
}
