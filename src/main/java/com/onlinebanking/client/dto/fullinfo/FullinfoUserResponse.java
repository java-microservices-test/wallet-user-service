package com.onlinebanking.client.dto.fullinfo;

import lombok.Data;

import java.util.List;
@Data
public class FullinfoUserResponse {
    private String fullname;

    private String userid;

    private String email;

    private String phone;

    private String accountNumber;

    private String expireDate;

    private String balance;

    private boolean isActive;
}
