package com.onlinebanking.client.dto.account;

import lombok.Data;

@Data
public class AccountDto {
    private String accountNumber;

    private String expireDate;

    private String balance;

    private boolean isActive;


}
