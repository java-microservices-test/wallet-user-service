package com.onlinebanking.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Random;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientDto {
    private String id;

    private String userid;

    private String fullname;

    private String email;

    private String phone;

    private String login;
    @JsonIgnore
    private String password;

    public String getUserid() {
       /* Random random = new Random();
        int randomUserid=random.nextInt(10000);
        String stringUserid =  String.format("%08d", new Random().nextInt(100000000));;
        userid = stringUserid;*/
        return userid;
    }

}
