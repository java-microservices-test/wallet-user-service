package com.onlinebanking.client.dto.register;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class RegisterRequest {
    private String login;
    private String password;
    private String email;
    private String phone;
    private String fullname;
}
