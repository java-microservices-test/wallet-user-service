package com.onlinebanking.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientAuthDataDto {
    private String userid;
    private String login;
    private String password;

    public String getUserid() {
        return userid;
    }
}
