package com.onlinebanking.client.dto.login;

import lombok.Data;

@Data
public class LoginResponse {
    private String login;
    private String password;


}
